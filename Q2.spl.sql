﻿SELECT * FROM user;

USE practice;

CREATE TABLE item_category(
category_id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
category_name varchar(256) NOT NULL
);


CREATE TABLE item(

item_id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
item_name varchar(256) NOT NULL,
item_price int NOT NULL,
category_id int
);

